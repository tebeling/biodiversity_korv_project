# Greetings !

### Welcome to the biodiversity and evolution - exercise I

> Koala (Phascolarctos cinereus) retrovirus (KoRV): 
identification of insertion sites and relationship to cancer related genes


This repo is a practice repo we use to manage the source files of our project. You can access this repo with SSH or with HTTPS.

Hereafter a short overview of the workflow and scripts that were used.

#### Workflow

1. Quality Control
2. String matching  
3. Calling
4. Comparison - Sequence identificatio


#### R- Package

We have developed a r package that you can easyly load to reproduce our workflow
and results

In the dev folder you can find a source or binary of our package "KoalReadAnalysis"

Just load it into your R library...

#### Dokumentation
 
For a full description ofthe functionalities of this package, 
load it into your R library and type:

help(KoalaReadAnalysis)

If you use rstudio you can go in you package section and select this package, 
to see a full list of all functions

#### How to use

We will provide a independent script, that will represent the
experiment workflow