#' Function will remove adapter contamination, as well as reads < 20 bp and trim reads regarding to a phredscore cutoff
#' 
#' @description Trim and filter reads
#' @usage getTrim("path_to_fastqc files","adapter_Sequence",min_Bp_length,cutoff_for_phredScore)
#' @param path to fastq files
#' @param Aseq Adapter Sequenced used to create read library - scripts works for single-read sequencing data.
#' @param minsize minimum size of reads after trimming
#' @param cutoff cutoff after which bp reads were cutted
#' @return fastq file with trimmed reads
#' @export
#' @examples
#' getTrim("/home/experiment", "AAGAGCACACGT", 20, 5)
getTrim <- function(path , Aseq, minSize, cutoff){
  #' import packages
  suppressMessages(require(ShortRead))
  suppressMessages(require(Biostrings))
  cat("Required packages are loaded...", "\n")
  
  fls <- dir(path, "*fastq", full = TRUE)
  
  #' define names
  names(fls) <- sub(".fastq.gz", "", basename(fls))
  cat("Start trimming", "\n")

  
  #' Define Adapter sequence via command line
  #' These are the adapter sequences
  
  #' AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
  Rpattern <- DNAString(Aseq)
  
  #' check reverse complement
  Rpattern_rev <- reverseComplement(Rpattern)
  cat("Processing...", "\n")
  #' These are vectors of equal length to the adapters with "nchar(adapter1)-10" for each value.
  #' This will allow the adapters to match the end of the sequence with any offset
  #' and up to nchar(adapter1)-10 mismatches
  #' 
  #' for all imported files
  for(i in 1:length(fls)){
    
    #' get sequence list
    reads <- readFastq(fls[i])  
    seqs <- sread(reads) 
    
    #' prepare quality storage
    qual <- quality(reads) 
    qual <- quality(qual) 
    
    #' Trim sequences looking for a right end pattern for adapter sequneces
    #' Gets IRanges object with trimmed coordinates 
    trimCoords <- trimLRPatterns(Rpattern = Rpattern, 
                                 subject = seqs, 
                                 max.Lmismatch = 0.3, 
                                 max.Rmismatch = 0.3, 
                                 ranges = T)
    seqs <- DNAStringSet(seqs, start = start(trimCoords), end = end(trimCoords))
    qual <- BStringSet(qual, start = start(trimCoords), end = end(trimCoords))
    
    #' Trim sequences looking for a right end pattern for reverse adapter sequences
    #' Gets IRanges object with trimmed coordinates 
    trimCoords <- trimLRPatterns(Rpattern = Rpattern_rev, 
                                 subject = seqs, 
                                 max.Lmismatch = 0.3, 
                                 max.Rmismatch = 0.3, 
                                 ranges = T)
    seqs <- DNAStringSet(seqs, start = start(trimCoords), end = end(trimCoords))
    qual <- BStringSet(qual, start = start(trimCoords), end = end(trimCoords))
    
    #' collecting all reads
    trimmed <- ShortReadQ(sread = seqs, quality = qual, id = id(reads))
    
    #' dumping files
    rm(seqs, qual, trimCoords, reads)
    
    #' trim reads if 10 bp occure with a phredscore <= 5 
    #' -> P([ACHI endcoded]) = 0.100 % (wrong base call)
    #' 
    trimmed <- trimTailw(trimmed, halfwidth = 10, k = 10, a = cutoff, 
                         successive = TRUE, 
                         left = FALSE, 
                         right = TRUE)    
    #' remove all reads shorter than minSize
    trimmed <- trimmed[minSize <= width(sread(trimmed))]
    
    #' Export Sequences
    #' 
    #'# write reads to fastq 
    writeFastq(trimmed, paste( fls[i], "_trimmed.fastq.gz"), Full = TRUE, withIds = TRUE)
    cat("\n", i, "/", length(fls), " - Fastqc file created.", sep = "", "\n" )
    #' user output
    cat(paste((i/length(10))*10, "% done...", sep = "", "\n","Path: ",  fls[i]), "\n")
    rm(trimmed)  
    }
  cat("Trimming done.", "\n")
  cat(paste("Your files are in: ", path), sep = "", "\n")
  cat("\n")
  rm(path, fls)
}