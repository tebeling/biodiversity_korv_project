#!/usr/bin/env Rscript
#' @Program Description:
#'  
#' Trim and filter reads
#'  
#' Workflow:
#' 
#' Use this script after script "getQualityControl.R"
#' 
#' Define your Adapter Sequences, Min bp length
#' and a cutoff for the phredscore
#' 
#' Script will create new fastq files with trimmed reads
#' you can check quality of new reads with script "getQualityControl.R"
#' 
#' Usage:
#' 
#' Rscript getTrim.R < path_to_fastqc files > < Adapter Sequence > < min bp length > < phredscore cutoff[asci] >
#' 
#' @date: 29.05.2014

#' define R working directory - only needed if you dont run
#'                              this script via command line
#' setwd("D:/Dropbox/Fu Berlin/Bioinformatik_Master/2_Sem/Biodiversitšt/korv_project/0_ raw_data/koala/raw")

#' import packages
require(ShortRead)
require(Biostrings)
#'
#' trimReads
#' 
#' Function will remove adapter contamination, as well as reads < 20 bp
#' and trim reads regarding to a phredscore cutoff of 20
#' 
#' @param path to fastq files
#' @param minsize minimum size of reads after trimming
#' @param cutoff cutoff after which bp reads were cutted
#' @return fastq file with trimmed reads
trimReads <- function(path , Aseq, minSize, cutoff){
  
  #' Define Adapter sequence via command line
  #' These are the adapter sequences
  #' AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
  Rpattern <- DNAString(Aseq)
  
  #' check reverse complement
  Rpattern_rev <- reverseComplement(Rpattern)
  
  #' These are vectors of equal length to the adapters with "nchar(adapter1)-10" for each value.
  #' This will allow the adapters to match the end of the sequence with any offset
  #' and up to nchar(adapter1)-10 mismatches
  #' 
  #' for all imported files
  for(i in 1:length(path)){
    
    print(paste("Processing File: ", i ,"Path: ",  path[i]))
    
    #' get sequence list
    reads <- readFastq(path[i])  
    seqs <- sread(reads) 
    
    #' prepare quality storage
    qual <- quality(reads) 
    qual <- quality(qual) 

    #' Trim sequences looking for a right end pattern for adapter sequneces
    #' Gets IRanges object with trimmed coordinates 
    trimCoords <- trimLRPatterns(Rpattern = Rpattern, 
                                 subject = seqs, 
                                 max.Lmismatch = 0.3, 
                                 max.Rmismatch = 0.3, 
                                 ranges = T)
    seqs <- DNAStringSet(seqs, start = start(trimCoords), end = end(trimCoords))
    qual <- BStringSet(qual, start = start(trimCoords), end = end(trimCoords))
    
    #' Trim sequences looking for a right end pattern for reverse adapter sequences
    #' Gets IRanges object with trimmed coordinates 
    trimCoords <- trimLRPatterns(Rpattern = Rpattern_rev, 
                                 subject = seqs, 
                                 max.Lmismatch = 0.3, 
                                 max.Rmismatch = 0.3, 
                                 ranges = T)
    seqs <- DNAStringSet(seqs, start = start(trimCoords), end = end(trimCoords))
    qual <- BStringSet(qual, start = start(trimCoords), end = end(trimCoords))
    
    #' collecting all reads
    trimmed <- ShortReadQ(sread = seqs, quality = qual, id = id(reads))
    
    #' dumping files
    rm(seqs, qual, trimCoords, reads)
    
    #' trim reads if 10 bp occure with a phredscore <= 5 
    #' -> P([ACHI endcoded]) = 0.100 % (wrong base call)
    #' 
    trimmed <- trimTailw(trimmed, halfwidth = 10, k = 10, a = cutoff, 
                          successive = TRUE, 
                          left = FALSE, 
                          right = TRUE)    
    #' remove all reads shorter than minSize
    trimmed <- trimmed[minSize <= width(sread(trimmed))]
    
    #' Export Sequences
    #' 
    #'# write reads to fastq 
    writeFastq(trimmed, paste( path[i], "_trimmed.fastq.gz"), Full = TRUE, withIds = TRUE) 
    rm(trimmed)
  }
  return(print(paste("Trimming done. Your files are in :", path[i] )))   
}

#' catch command line argument
args <- commandArgs(TRUE)
args_fls  <- args[1]
args_seq  <- args[2]
args_minL <- as.integer(args[3])
args_Cp   <- as.integer(args[4])
rm(args)

#' Importing Fastq files of previous quality control step
#' and trim them
#' 
#' import path of fastq files
fls <- dir(args_fls, "*fastq", full = TRUE)
#' define names
names(fls) <- sub(".fastq.gz", "", basename(fls))
cat("Start trimming", "\n")
#' call trim function
#' 
#' We will cut after the 145 bp, 
#' because of the dropping phredscore
#' cutoff is "5" ASCI encoded
#' and windowssize for moving average is 2 * 10 + 1
trimReads(fls, args_seq, args_minL, args_Cp)
rm(fls)