#!/usr/bin/env Rscript
#' @Program Description:
#'  
#' Apply a quality control over reads in fastq files, 
#' specified by user
#' 
#' Report is saved in separted folder "QC_report"
#' Results are also saved as an R object called "qc.Rdata" 
#' 
#' Workflow:
#' 
#' We will start our Experiment here. First we apply a quality control
#' over given data to respect the theorem: "Garbage in. Garbage out"
#' 
#' run script via command line:
#' 
#' Rscript getQualityCOntrol <path_to_fastqc>
#' 
#' @date: 28.05.2014

#' catch command line argument
args <- commandArgs(TRUE)
args_fls <- args[1]


#' define R working directory - only needed if you dont run
#'                              this script via command line
#' setwd("D:/Dropbox/Fu Berlin/Bioinformatik_Master/2_Sem/Biodiversitšt/korv_project/0_ raw_data/koala")
#' import packages
#' 
#' First you need to install the packages:
#' 
#' run this:
#' 
#' source("http://bioconductor.org/biocLite.R")
#' biocLite("ShortRead")
#' biocLite("Biostrings")
require(ShortRead)
require(Biostrings)
#' import fastq files
#' Here we have 5 samples: 
#' each one tumor one healty tissue sample
#' 
#' -> we will import samples in following order
#'    1. healthy
#'    2. tumor
#'    
#'    define path to fastqc.gz/fastqc files 
#     use this if you dont call via command line

#' fls <- dir("D:/Dropbox/Fu Berlin/Bioinformatik_Master/2_Sem/Biodiversitšt/korv_project/0_ raw_data/koala/raw", 
#'           full = TRUE)
fls <- dir(args_fls, full = TRUE)

#' store path to working directory
wd <- getwd()
#' set names
names(fls) <- sub(".fastq.gz", "", basename(fls))
cat("\n", "Start quality control", "\n", "Processing...", "\n", sep = "")
#' quality control (qc) for all input files
qas <- lapply(seq_along(fls),
              function(i, fls) qa(readFastq(fls[i]), names(fls)[i]), fls)
#' apply qc on all files
qa <- do.call(rbind, qas)
cat("Quality control done." , "\n", "Your files are in: ", wd, sep = "") 
#' save report to html file
Nwd <- paste(wd, "/QC_report", sep = "")
report_html(qa, dest = "QC_report")
cat("\n", "Directory 'QC_Report' is created", sep = "")
cat("\n", "Report is created and saved." ,sep = "")
cat("\n", "Files are in: ", Nwd, sep = "")
#' save report
save(qa, file = "qa.rda")
cat("\n", "QC R- object is created.", sep = "")
cat("\n", "Files are in: " , wd, sep = "")
#'show report
cat("\n", "Quality control done.", sep = "")
#' remove used files
rm(Nwd, wd, qas, qa, fls)